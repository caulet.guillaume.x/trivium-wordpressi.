<?php get_header() ?>
<h2>Index</h2>
<main>
    <?php $wp_all_query = new WP_Query(array('post_type'=> 'post', 'post_status'=> 'publish','post_per_page' => -1, 'orderby'=> 'title', 'order'=> 'ASC'));
     ?>
    
        <?php if ( have_posts()) : 
        $previousLetter = ""; ?>
            <div class="row">
            <?php while ($wp_all_query->have_posts()) : $wp_all_query->the_post(); ?>
                <?php
                $nom = get_the_title();
                $lettre = substr($nom, 0, 1);
                if (strcmp($previousLetter, $lettre) !== 0) {
                    $previousLetter = $lettre;
                    echo "<h2>" . $lettre . "</h2>";
                }
                ?>

                <div class="col-sm-6">
                    <div class="card" style="width: 18rem;">
                        <h5 class="card-title"><?php the_title() ?></h5>
                        <?php the_post_thumbnail() ?>
                        <div class="card-body">
                            <p class="card-text"><?php the_excerpt() ?></p>
                            <a href="<?php the_permalink() ?>" class="btn btn-primary" title="Voir L'article">Détails</a>
                        </div>
                    </div>
                </div>


        <?php endwhile;
        endif; ?>
    </div>
</main>
<?php wp_footer() ?>