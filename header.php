<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Voici un site wordpress regroupant des informations sur quelques animaux comme leur nom leur taille une image une decription ainsi que leur régime alimentaire">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="https://w7.pngwing.com/pngs/638/516/png-transparent-fashion-t-shirt-casual-supera-rx-b-logo-cosmetics-fashion-logo.png" />
    <link rel="stylesheet" href="/wp-content/themes/trivium/style.css">
    <link href="https://fonts.googleapis.com/css2?family=MedievalSharp&display=swap" rel="stylesheet">
    <?php wp_head() ?>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <header>
        <nav class="navbar navbar-light bg-light">
            <div class="container">
                <a title="Page D'accueil" href="/index.php"><img src="https://i.ibb.co/TtyB0RT/Uroboros-removebg-preview.png" alt="logo" width="150" height="144" ></a>
                <h1>Le Bestiaire</h1>
            </div>
        </nav>
    </header>
<div class="container">