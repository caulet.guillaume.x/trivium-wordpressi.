<?php get_header() ?>

<?php $featured_posts = get_field('regime_alimentaire'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <main>
            <div class="containerCard">
                <div class="card" style="width: 18rem;">
                    <h5 class="card-title"><?php the_title(); ?></h5>
                    <?php the_post_thumbnail(); ?>
                    <div class="card-body">
                    <div class="gras" >Taille : <?php the_field('taille'); ?> Toises</div>
                        <p class="card-text"><?php the_content(); ?></p>
                    
                    <p class="gras">Régime Alimentaire</p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <?php foreach ($featured_posts as $post) :
                            setup_postdata($post); ?>
                            <li class="list-group-item"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </main>
<?php endwhile;
endif;
wp_footer() ?>